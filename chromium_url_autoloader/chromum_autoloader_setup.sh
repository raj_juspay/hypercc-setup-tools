#!/bin/bash
DIR="/tmp"

mkdir -p $DIR/.config/autostart
cat <<EOA >> $DIR/.config/autostart/kiosk.desktop
[Desktop Entry]
Type=Application
Name=Kiosk
Exec=$DIR/kiosk.sh
X-GNOME-Autostart-enabled=true
EOA

cat <<EOM >> $DIR/kiosk.sh
#!/bin/bash
# Run this script in display 0 - the monitor
export DISPLAY=:0
# Hide the mouse from the display
unclutter &
# If Chrome crashes (usually due to rebooting), clear the crash flag so we don't have the annoying warning bar
sed -i 's/"exited_cleanly":false/"exited_cleanly":true/' $DIR/.config/chromium/Default/Preferences
sed -i 's/"exit_type":"Crashed"/"exit_type":"Normal"/' $DIR/.config/chromium/Default/Preferences
# Run Chromium and open tabs
chromium-browser --kiosk $1 
sleep 10s
chromium-browser $2

EOM

chmod +x $DIR/kiosk.sh
